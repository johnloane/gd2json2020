package com.dkit.gd2.johnloane;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

public class MainApp
{
    public static void main(String[] args)
    {
        ObjectMapper objectMapper = new ObjectMapper();

        Car car = new Car("Convertible", 4, 2, "Boxster", "Prosche", 140, "Diesel", 3.0, 4, "Black");

        writeCarJSONToFile(objectMapper, car);
        writeCarJSONToString(objectMapper, car);
        createCarFromJSONString(objectMapper);
        createCarFromFile(objectMapper);
        testJsonNode(objectMapper);
        createListFromJSONArrayString(objectMapper);
        createMapFromJSONString(objectMapper);
        deserializeJSONStringExtraFields(objectMapper);
        useCustomSerializer();
        useCustomDeserializer();
        testDateFormatter();
        deserializeToArray();
    }

    public static void writeCarJSONToFile(ObjectMapper mapper, Car car)
    {
        try
        {
            mapper.writeValue(new File("car.json"), car);
        }
        catch(JsonGenerationException e)
        {
            System.out.println(e.getMessage());
        }
        catch(JsonMappingException e)
        {
            System.out.println(e.getMessage());
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
    }

    public static void writeCarJSONToString(ObjectMapper mapper, Car car)
    {
        try {
            String carAsString = mapper.writeValueAsString(car);
            System.out.println(carAsString);
        }
        catch(JsonProcessingException e)
        {
            System.out.println(e.getMessage());
        }

    }

    public static Car createCarFromJSONString(ObjectMapper mapper)
    {
        Car car = null;
        String json = "{\"body\":\"Convertible\",\"wheels\":4,\"doors\":2,\"model\":\"Boxster\",\"make\":\"Prosche\",\"topSpeed\":140,\"fuelType\":\"Diesel\",\"engineSize\":3.0,\"noughtToSixty\":4,\"colour\":\"Black\"}";
        try {
            car = mapper.readValue(json, Car.class);
            System.out.println(car);
        }
        catch(JsonMappingException e)
        {
            System.out.println(e.getMessage());
        }
        catch(JsonProcessingException e)
        {
            System.out.println(e.getMessage());
        }
        return car;

    }

    public static Car createCarFromFile(ObjectMapper mapper)
    {
        Car car = null;
        try {
            car = mapper.readValue(new File("car.json"), Car.class);
            System.out.println(car);
        }
        catch(JsonMappingException e)
        {
            System.out.println(e.getMessage());
        }
        catch(JsonProcessingException e)
        {
            System.out.println(e.getMessage());
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
        return car;
    }

    public static void testJsonNode(ObjectMapper mapper)
    {
        String json = "{\"body\":\"Convertible\",\"wheels\":4,\"doors\":2,\"model\":\"Boxster\",\"make\":\"Prosche\",\"topSpeed\":140,\"fuelType\":\"Diesel\",\"engineSize\":3.0,\"noughtToSixty\":4,\"colour\":\"Black\"}";
        try {
            JsonNode jsonNode = mapper.readTree(json);
            String colour = jsonNode.get("colour").asText();
            System.out.println("Colour: " + colour);
        }
        catch(JsonMappingException e)
        {
            System.out.println(e.getMessage());
        }
        catch(JsonProcessingException e)
        {
            System.out.println(e.getMessage());
        }

    }

    public static void createListFromJSONArrayString(ObjectMapper mapper)
    {
        String jsonCarArray = "[{\"body\":\"Convertible\",\"wheels\":4,\"doors\":2,\"model\":\"Boxster\",\"make\":\"Prosche\",\"topSpeed\":140,\"fuelType\":\"Diesel\",\"engineSize\":3.0,\"noughtToSixty\":4,\"colour\":\"Black\"}, {\"body\":\"SUV\",\"wheels\":4,\"doors\":4,\"model\":\"Discovery\",\"make\":\"Landrover\",\"topSpeed\":140,\"fuelType\":\"Diesel\",\"engineSize\":3.0,\"noughtToSixty\":6,\"colour\":\"Black\"}]";

        try
        {
            List<Car> carList = mapper.readValue(jsonCarArray, new TypeReference<List<Car>>() {});
            printList(carList);
        }
        catch(JsonMappingException e)
        {
            System.out.println(e.getMessage());
        }
        catch(JsonProcessingException e)
        {
            System.out.println(e.getMessage());
        }
    }

    public static void printList(List<Car> carList)
    {
        for(Car car : carList)
        {
            System.out.println(car);
        }
    }

    public static void createMapFromJSONString(ObjectMapper mapper)
    {
        String json = "{\"colour\" : \"Black\", \"type\" : \"BMW\"}";
        try
        {
            Map<String, Object> carMap = mapper.readValue(json, new TypeReference<Map<String, Object>>() {});
            printMap(carMap);
        }
        catch(JsonMappingException e)
        {
            System.out.println(e.getMessage());
        }
        catch(JsonProcessingException e)
        {
            System.out.println(e.getMessage());
        }
    }

    public static void printMap(Map<String, Object> cMap)
    {
        for(Map.Entry<String, Object> entry : cMap.entrySet())
        {
            System.out.println(entry.getKey() + ": " + entry.getValue().toString());
        }
    }

    public static void deserializeJSONStringExtraFields(ObjectMapper mapper)
    {
        String json = "{\"body\":\"Convertible\",\"wheels\":4,\"doors\":2,\"model\":\"Boxster\",\"make\":\"Prosche\",\"topSpeed\":140,\"fuelType\":\"Diesel\",\"engineSize\":3.0,\"noughtToSixty\":4,\"colour\":\"Black\", \"bootSize\":\"Medium\"}";

        try
        {
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            Car car = mapper.readValue(json, Car.class);
            System.out.println("Deserialized car: " + car);

            JsonNode jsonNodeRoot = mapper.readTree(json);
            JsonNode jsonNodeBootSize = jsonNodeRoot.get("bootSize");
            String bootSize = jsonNodeBootSize.asText();
            System.out.println(bootSize);
        }
        catch(JsonMappingException e)
        {
            System.out.println(e.getMessage());
        }
        catch(JsonProcessingException e)
        {
            System.out.println(e.getMessage());
        }
    }

    public static void useCustomSerializer()
    {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule("CustomCarSerializer", new Version(1,0,0,null, null, null));
        module.addSerializer(Car.class, new CustomCarSerializer());
        mapper.registerModule(module);
        Car car = new Car("Convertible", 4, 2, "Boxster", "Prosche", 140, "Diesel", 3.0, 4, "Black");
        try
        {
            String carJson = mapper.writeValueAsString(car);
            System.out.println("Custom version: " + carJson);
        }
        catch(JsonProcessingException e)
        {
            System.out.println(e.getMessage());
        }
    }

    public static void useCustomDeserializer()
    {
        String json = "{\"body\":\"Convertible\",\"wheels\":4,\"doors\":2,\"model\":\"Boxster\",\"make\":\"Prosche\",\"topSpeed\":140,\"fuelType\":\"Diesel\",\"engineSize\":3.0,\"noughtToSixty\":4,\"colour\":\"Black\", \"bootSize\":\"Medium\"}";

        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule("CustomCarDeserializer", new Version(1,0,0,null, null, null));
        module.addDeserializer(Car.class, new CustomCarDeserializer());
        mapper.registerModule(module);

        try
        {
            Car car = mapper.readValue(json, Car.class);
            System.out.println(car);
        }
        catch(JsonMappingException e)
        {
            System.out.println(e.getMessage());
        }
        catch(JsonProcessingException e)
        {
            System.out.println(e.getMessage());
        }
    }

    public static void testDateFormatter()
    {
        ObjectMapper mapper = new ObjectMapper();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm a z");
        mapper.setDateFormat(df);

        Car car = new Car("Convertible", 4, 2, "Boxster", "Prosche", 140, "Diesel", 3.0, 4, "Black");
        Request request = new Request(car);
        try
        {
            String requestAsString = mapper.writeValueAsString(request);
            System.out.println(requestAsString);
        }
        catch(JsonProcessingException e)
        {
            System.out.println(e.getMessage());
        }
    }

    public static void deserializeToArray()
    {
        String jsonCarArray = "[{\"body\":\"Convertible\",\"wheels\":4,\"doors\":2,\"model\":\"Boxster\",\"make\":\"Prosche\",\"topSpeed\":140,\"fuelType\":\"Diesel\",\"engineSize\":3.0,\"noughtToSixty\":4,\"colour\":\"Black\"}, {\"body\":\"SUV\",\"wheels\":4,\"doors\":4,\"model\":\"Discovery\",\"make\":\"Landrover\",\"topSpeed\":140,\"fuelType\":\"Diesel\",\"engineSize\":3.0,\"noughtToSixty\":6,\"colour\":\"Black\"}]";

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.USE_JAVA_ARRAY_FOR_JSON_ARRAY, true);
        try
        {
            Car[] cars = mapper.readValue(jsonCarArray, Car[].class);
            for(Car car : cars)
            {
                System.out.println(car);
            }
        }
        catch(JsonMappingException e)
        {
            System.out.println(e.getMessage());
        }
        catch(JsonProcessingException e)
        {
            System.out.println(e.getMessage());
        }
    }
}
