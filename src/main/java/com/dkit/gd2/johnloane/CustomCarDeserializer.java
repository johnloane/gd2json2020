package com.dkit.gd2.johnloane;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;

public class CustomCarDeserializer extends StdDeserializer<Car>
{
    public CustomCarDeserializer()
    {
        this(null);
    }

    public CustomCarDeserializer(Class<Car> t)
    {
        super(t);
    }

    @Override
    public Car deserialize(JsonParser parser, DeserializationContext deserializer)
    {
        Car car = new Car();
        ObjectCodec codec = parser.getCodec();

        try
        {
            JsonNode node = codec.readTree(parser);
            JsonNode colourNode = node.get("colour");
            String colour = colourNode.asText();
            car.setColour(colour);
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
        return car;
    }
}
